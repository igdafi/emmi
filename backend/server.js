// Hello world!
console.log('Getting Emmi ready for the day...')

// Initialise
// ------------------

const config = require('./config')

module.exports.config = config

if (process.env.NODE_ENV === 'production') {
  config.productionEnv = true
  console.log('Config set to PRODUCTION')
} else console.log('Config set to DEVELOPMENT')

console.log('Initializing Express and Mongoose...')
const express = require('express')
const fallback = require('express-history-api-fallback')
const compression = require('compression')
const app = express()
const helmet = require('helmet')
app.use(helmet({
  crossOriginEmbedderPolicy: false,
  crossOriginResourcePolicy: {
    policy: 'cross-origin'
  },
  contentSecurityPolicy: {
    directives: {
      'default-src': ["'self'"],
      'script-src': ["'self'", "'unsafe-inline'", "'unsafe-eval'", 'js.stripe.com'],
      'object-src': ["'none'"],
      'img-src': ["'self'", '*.slack-edge.com', 'data:'],
      'font-src': ["'self'", 'js.stripe.com', 'fonts.gstatic.com'],
      'style-src': ["'self'", "'unsafe-inline'", 'fonts.googleapis.com'],
      'frame-src': ['js.stripe.com', 'https://hooks.stripe.com'],
      'upgrade-insecure-requests': [],
      'connect-src': ['self', 'https://api.stripe.com']
    }
  }
}))
app.use(compression())
const mongoose = require('mongoose')
mongoose.Promise = require('bluebird')

console.log('Initializing cron jobs...')
const CronJob = require('cron').CronJob

module.exports = { app, config }

// Cron job for membership expiration emails
// eslint-disable-next-line no-new
new CronJob({
  cronTime: config.cron.membershipEmailsString,
  onTick: function () {
    console.log('Triggering monthly membership emails...')
    dbApi.sendMonthlyEmails()
  },
  start: true
})

console.log('Initializing Nodemailer...')
module.exports.nodemailer = require('./memberEmails')

console.log('Initializing MailChimp...')
module.exports.mailchimp = require('./mailchimpManager')

console.log('Initializing Slack...')
const slack = require('./slack')

console.log('Initializing API module...')
const dbApi = require('./routes/dbapi')

// Start
// ------------------

let server
console.log('Connecting to the database...')

// let mongostring = 'mongodb://'
// if (config.secrets.database.user) mongostring += config.secrets.database.user + ':' + config.secrets.database.pass + '@'
// mongostring += config.secrets.database.url + ':' + config.secrets.database.port + '/' + config.secrets.database.dbName

const mongoOptions = {
  useNewUrlParser: true,
  useUnifiedTopology: true
}

mongoose.connect(config.secrets.database.connectionString, mongoOptions)

mongoose.connection.on('error', function (error) {
  console.error('Error in MongoDb connection: ' + error)
  mongoose.disconnect()
})
mongoose.connection.on('disconnected', function () {
  console.log('MongoDB disconnected! Retrying in 5 sec...')
  setTimeout(function () { mongoose.connect(config.secrets.database.connectionString, mongoOptions) }, 5000)
})
mongoose.connection.once('open', function callback () {
  console.log('Connected to database!')

  // REST API routing
  app.use('/api', dbApi.router)

  // All other requests
  const path = require('path')
  app.use('/card', express.static(path.join(__dirname, '/public_html_cards')))
  app.use('/card', fallback(path.join(__dirname, '/public_html_cards/index.html')))

  app.use(express.static(path.join(__dirname, '/public_html_emmi')))
  app.use(fallback(path.join(__dirname, '/public_html_emmi/index.html')))

  // Open HTTP now that we have a db connection
  server = app.listen(process.env.PORT || 8080, function () {
    console.log('HTTP open: listening on port %d', server.address().port)
    slack.post('All booted up and ready to go!\nVersion: ' + config.version, false, [{
      fallback: 'Patch notes: https://emmi.igda.fi/patchnotes',
      actions: [
        {
          type: 'button',
          name: 'visit emmi',
          text: 'Open Emmi',
          url: 'https://emmi.igda.fi',
          style: 'primary'
        },
        {
          type: 'button',
          name: 'patch notes',
          text: "What's new?",
          url: 'https://emmi.igda.fi/patchnotes'
        }]
    }])
  })
})

// Stop
// ------------------

process.on('SIGTERM', () => {
  console.info('SIGTERM signal received')
  slack.post('Received a kill signal from the OS (SIGTERM). Shutting down...', false, [{
    fallback: 'Logs: https://console.cloud.google.com/logs/viewer?project=reliable-mender-102215&resource=gce_instance%2Finstance_id%2F8736440210746052274',
    actions: [
      {
        type: 'button',
        name: 'view logs',
        text: 'View logs',
        url: 'https://console.cloud.google.com/logs/viewer?project=reliable-mender-102215&resource=gce_instance%2Finstance_id%2F8736440210746052274'
      }]
  }], () => {
    server.close(() => {
      console.log('Http server closed')
      mongoose.connection.close(false, () => {
        console.log('MongoDb connection closed')
        process.exit(0)
      })
    })
  })
})

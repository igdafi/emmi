const mongoose = require('mongoose')
const passport = require('passport')
const cookieParser = require('cookie-parser')
const session = require('express-session')
const MongoStore = require('connect-mongo')
const SlackStrategy = require('passport-slack').Strategy
const server = require('../server')
const app = server.app
const config = server.config

// Mongoose user model (note: roles are 1=new user, 2=volunteer, 3=board, 4=admin, 5=developer)
const User = mongoose.model('User', {
  _id: String,
  name: String,
  role: Number,
  roleName: String
})

// Serialize and deserialize
passport.serializeUser(function (user, done) {
  done(null, { _id: user._id, name: user.name, role: user.role, roleName: user.roleName })
})
passport.deserializeUser(function (id, done) {
  User.findById(id, function (err, user) {
    if (!err) done(null, user)
    else done(err, null)
  })
})

let callbackURL
if (config.productionEnv) callbackURL = 'https://emmi.igda.fi/api/auth/login/callback'
else callbackURL = 'http://localhost:8080/api/auth/login/callback'

// Config
passport.use(new SlackStrategy({
  clientID: config.secrets.loginProviders.slack.clientID,
  clientSecret: config.secrets.loginProviders.slack.clientSecret,
  callbackURL,
  scope: ['identity.basic']
},
(accessToken, refreshToken, profile, done) => {
  User.findById(profile.id, function (err, user) {
    if (err) console.log(err)

    if (!err && user !== null) {
      console.log('User logged in: ' + profile.displayName)
      done(null, user)
    } else {
      const newUser = new User({
        _id: profile.id,
        name: profile.displayName,
        role: 1,
        roleName: 'new user'
      })
      newUser.save(function (err) {
        if (err) console.log(err)
        else {
          console.log('Creating a new user: ' + profile.displayName)
          done(null, newUser)
        }
      })
    }
  })
}
))

app.set('trust proxy', 1)
app.use(cookieParser(config.secrets.cookieSecret))
app.use(session({
  secret: config.secrets.cookieSecret,
  name: 'session',
  store: MongoStore.create({
    mongoUrl: config.secrets.database.connectionString,
    mongoOptions: { clientPromise: mongoose.connection.getClient() }
  }),
  saveUninitialized: true,
  resave: false,
  cookie: {
    maxAge: 1000 * 60 * 60 * 24 * 7,
    rolling: true
  }
}))
app.use(passport.initialize())
app.use(require('body-parser').urlencoded({ extended: true }))
app.use(passport.session())

module.exports = {
  getUser: function (req, res) {
    if (req.isAuthenticated()) res.send(req.user)
    else {
      res.clearCookie('user')
      res.send('0')
    }
  },

  // This gets called on login...
  login: passport.authenticate('slack'),

  // ...then this triggers after coming back from OAuth...
  loginCallback1: passport.authenticate('slack', { failureRedirect: '/auth' }),

  // ...and finally we are logged in for sure
  loginCallback2: function (req, res) {
    // res.cookie('user', JSON.stringify({ name: req.session.passport.user.name, role: req.session.passport.user.role, roleName: req.session.passport.user.roleName }), { maxAge: 1000 * 60 * 60 * 24 * 7 })
    res.redirect('/')
  },

  logout: function (req, res) {
    req.logout()
    res.clearCookie('user')
    res.redirect('/')
  },

  getAllUsers: function (req, res) {
    // eslint-disable-next-line array-callback-return
    User.find(function (err, users) {
      if (err) res.status(500).send(err)
      else res.json(users)
    })
  },

  getAllAdmins: function (req, res) {
    User.find({ role: { $gt: 3 } }, function (err, admins) {
      if (err) res.status(500).send(err)
      else res.json(admins)
    })
  },

  setUser: function (req, res) {
    let newRoleName
    if (req.body.newRole === '1') newRoleName = 'new user'
    else if (req.body.newRole === '2') newRoleName = 'volunteer'
    else if (req.body.newRole === '3') newRoleName = 'board'
    else if (req.body.newRole === '4') newRoleName = 'admin'
    else if (req.body.newRole === '5') newRoleName = 'developer'
    User.findByIdAndUpdate({ _id: req._id }, { role: req.body.newRole, roleName: newRoleName }, { new: true }, function (err, user) {
      if (err) res.status(500).send(err)
      else if (user == null) res.status(400).send('User not found')
      else res.json(user)
    })
  },

  deleteUser: function (req, res) {
    User.findByIdAndDelete({ _id: req._id }, (err, res2) => {
      if (err) res.status(500).send(err)
      else if (res2 == null) res.status(400).send('User not found')
      else {
        console.log('User deleted')
        res.json(res2)
      }
    })
  }
}

const mongoose = require('mongoose')
const _ = require('lodash')
const eventSchema = require('./dbapi').eventSchema
const eventOrganizerSchema = require('./dbapi').eventOrganizerSchema
const Visitor = mongoose.model('Visitor')

eventSchema.methods.getVisitors = function (res) {
  const checkinTimes = {}
  const visitorIds = _.map(this.visitors,
    function (x) { return mongoose.Types.ObjectId(x.id) })

  for (const ii in this.visitors) {
    const id = this.visitors[ii].id
    checkinTimes[id] = this.visitors[ii].checkin
  }

  Visitor.find({ _id: { $in: visitorIds } }, function (err, visitors) {
    if (err) {
      console.error(err)
      res.status(500).send(err)
    }

    for (const ii in visitors) {
      const id = visitors[ii].id
      visitors[ii] = { info: visitors[ii], checkin: checkinTimes[id] }
    }

    res.json(visitors)
  })
}

const Event = mongoose.model('Event', eventSchema)
const EventOrganizer = mongoose.model('EventOrganizer', eventOrganizerSchema)

module.exports = {
  getAll: function (req, res) {
    Event.find(function (err, events) {
      if (err) {
        res.status(500).send(err)
      } else {
        res.json(events)
      }
    })
  },

  getAllHeadlines: function (req, res) {
    Event.find({}, 'name location organizer sponsor date', function (err, events) {
      if (err) {
        res.status(500).send(err)
      } else {
        res.json(events)
      }
    })
  },

  getById: function (req, res) {
    Event.findById(req._id, function (err, event) {
      if (err) {
        res.status(500).send(err)
      } else {
        res.json(event)
      }
    })
  },

  getAllOrganizers: function (req, res) {
    EventOrganizer.find(function (err, result) {
      if (err) {
        res.status(500).send(err)
      } else {
        res.json(result)
      }
    })
  },

  create: function (req, res) {
    const event = new Event(req.body)

    event.save(function (err, result) {
      if (err) {
        res.status(500).send(err)
      } else {
        res.json(result)
      }
    })
  },

  update: function (req, res) {
    Event.findByIdAndUpdate(req._id, { $set: req.body },
      function (err, event) {
        if (err) {
          res.status(500).send(err)
        } else {
          res.json(event)
        }
      })
  },

  delete: function (req, res) {
    Event.findByIdAndRemove(req._id, function (err, event) {
      if (err) {
        res.status(500).send(err)
      } else {
        res.status(200).send()
      }
    })
  },

  createOrganizer: function (req, res) {
    const eventOrganizer = new EventOrganizer(req.body)

    eventOrganizer.save(function (err, result) {
      if (err) {
        res.status(500).send(err)
      } else {
        res.json(result)
      }
    })
  },

  updateOrganizer: function (req, res) {
    EventOrganizer.findByIdAndUpdate(req._id, { $set: req.body },
      function (err, result) {
        if (err) {
          res.status(500).send(err)
        } else {
          res.json(result)
        }
      })
  },

  deleteOrganizer: function (req, res) {
    EventOrganizer.findByIdAndRemove(req._id, function (err, result) {
      if (err) {
        res.status(500).send(err)
      } else {
        res.status(200).send(result)
      }
    })
  },

  getVisitors: function (req, res) {
    Event.findById(req._id, function (err, event) {
      if (err) {
        res.status(500).send(err)
      } else {
        event.getVisitors(res)
      }
    })
  },

  // Note: doesn't validate that user id exists. Kinda pointless.
  addVisitor: function (req, res) {
    Event.findById(req._id, function (err, event) {
      if (err) {
        console.error('Error finding event to check-in someone: ' + JSON.stringify(err))
        res.status(500).send(err)
      } else {
        const visitor = { id: req.body.id, checkin: Date.now() }

        let duplicate = false
        for (const v of event.visitors) {
          if (v.id === visitor.id) {
            duplicate = true
            break
          }
        }
        if (!duplicate) {
          event.visitors.push(visitor)
          event.save(function (err2) {
            if (err2) {
              console.error('Error checking visitor in to event: ' + JSON.stringify(err2))
              res.status(500).send(err2)
            } else {
              res.json({ updatedEvent: event, result: 'ok' })
            }
          })
        } else {
          res.json({ updatedEvent: event, result: 'duplicate' })
        }
      }
    })
  },

  removeVisitor: function (req, res) {
    Event.findById(req._id, function (err, event) {
      if (err) {
        console.error('Error finding event to check-out someone: ' + JSON.stringify(err))
        res.status(500).send(err)
      } else {
        let removed = false
        for (let i = 0; i < event.visitors.length; i++) {
          if (event.visitors[i].id === req.body.id) {
            event.visitors.splice(i, 1)
            removed = true
            break
          }
        }

        if (removed) {
          event.save(function (err2) {
            if (err2) {
              console.error('Error removing visitor from event: ' + JSON.stringify(err2))
              res.status(500).send(err2)
            } else {
              res.json({ updatedEvent: event, result: 'ok' })
            }
          })
        } else {
          res.json({ updatedEvent: event, result: 'not found' })
        }
      }
    })
  }
}

#!/usr/bin/env bash

VERSION=$(node -p "require('./backend/package.json').version")
echo "Publishing as" ${VERSION}
git tag ${VERSION}
docker build -t registry.gitlab.com/igdafi/emmi:latest .
docker tag registry.gitlab.com/igdafi/emmi:latest registry.gitlab.com/igdafi/emmi:${VERSION}
docker push registry.gitlab.com/igdafi/emmi:latest
docker push registry.gitlab.com/igdafi/emmi:${VERSION}
echo "All done!"
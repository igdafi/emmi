# Membership Cards Frontend Client

This is a separate piece of code to avoid any overlap between Emmi and cards since they actually shouldn't share any. Basically this code just consumes the public Emmi API to display a subset of membership info in a nice way.

## Development

* `npm run serve` to run locally for easy iteration
* `localhost:8080/card/FAKE` will work off of a dummy db response